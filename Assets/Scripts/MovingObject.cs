using UnityEngine;
using System.Collections;


public class MovingObject : MonoBehaviour {
  public float refreshTime = 0.05f;
  public LayerMask blockingLayer;

  //protected BoxCollider2D box;
  //protected Rigidbody2D body;
  protected CharacterController character;
  private float inverseRefreshTime;

  protected virtual void Start() {
    //box = GetComponent<BoxCollider2D>();
    //body = GetComponent<Rigidbody2D>();
    character = GetComponent<CharacterController>();
    inverseRefreshTime = 1f / refreshTime;      
  }
    
  protected bool Move(float xDir, float yDir, out RaycastHit2D hit) {
    Vector2 start = transform.position;
    Vector2 end = start + new Vector2(xDir, yDir);
    //box.enabled = false;
    hit = Physics2D.Linecast(start, end, blockingLayer);
    //box.enabled = true;

    if (hit.transform == null) {
      //body.velocity = new Vector2(xDir, yDir);//new Vector2(Mathf.Lerp(0, xDir, 0.3f), Mathf.Lerp(0, yDir, 0.3f));
      character.Move(new Vector3(Mathf.Lerp(0, xDir, 0.3f), Mathf.Lerp(0, yDir, 0.3f), 0));
      return true;
    }
    return false;
  }

  protected virtual void AttemptMove(float xDir, float yDir) {
    RaycastHit2D hit;
    bool canMove = Move(xDir, yDir, out hit);

    if (hit.transform == null)
      return;      
  }

}
