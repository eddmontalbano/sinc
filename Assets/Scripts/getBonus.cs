﻿using UnityEngine;
using System.Collections;
using Game;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class getBonus : MonoBehaviour {
  // Use this for initialization
	void Start () {
    Text text = GetComponent<Text>();
    if (GameManager.instance.hamster == true) {
      print("hamster!");
      text.text = "50";
      GameManager.instance.hamster = false;
    } else {
      text.text = "0";
    }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
