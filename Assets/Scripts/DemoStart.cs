﻿using UnityEngine;
using System.Collections;

public class DemoStart : MonoBehaviour {
  public bool touched = false;
  public GameObject player;
  private Animator animator;
  private int message = 0;
  private Vector3 touchPosition;

  public GameObject cup;
  public GameObject gameCup;

  public float counter = 0;

  void Awake() {
    animator = player.GetComponent<Animator>();    
  }

  private void FixedUpdate() {
#if UNITY_STANDALONE || UNITY_STANDALONE_WIN

    if (Input.GetButton("Fire1")) {
      touched = true;
      touchPosition = Input.mousePosition;
      if (message == 2) { //Make user play with the cup
        //Vector3 tp = Camera.main.ScreenToWorldPoint(touchPosition);
        //gameCup.transform.position = new Vector3(tp.x, tp.y, -2f);
      }
    } else if (message == 3) {
      gameCup.transform.position = new Vector3(0f, 0f, -9999f);
      Application.LoadLevel("sinc");
    } else if (touched && message != 2) {
      touched = false;
      message++;
    }
#else
    if (Input.touchCount > 0) {
      touched = true;
      touchPosition = Input.GetTouch(0).position;
      if (message == 2) {
        Vector3 tp = Camera.main.ScreenToWorldPoint(touchPosition);
        gameCup.transform.position = new Vector3(tp.x, tp.y, -2f);
      }
    } else if (message == 2) { 
      gameCup.transform.position = new Vector3(0f, 0f, -9999f);
      message++;
    }
#endif

    if (touched) {
      if (message == 0) { // Rules Suck
        animator.SetTrigger("next");
      } else if (message == 1) {
        animator.SetTrigger("next");
      } else if (message == 2) { // List rules, touch the cup and play with it once.

        int layerMask = 1 << 8;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touchPosition), Vector2.zero, Mathf.Infinity, layerMask);
        if (hit.collider != null) {
          hit.collider.gameObject.SetActive(false);
          message++;
        }
      } else if (message == 3) { //Make user play with the cup
        //Vector3 tp = Camera.main.ScreenToWorldPoint(touchPosition);
        //gameCup.transform.position = new Vector3(tp.x, tp.y, -2f);
        animator.SetTrigger("next");
      } else if (message == 3) {
        animator.SetTrigger("next");
        message++;
      }
    }
    if (message == 4) {
      counter += Time.deltaTime;
    }
    if (counter > 5.0f) {
        Application.LoadLevel("sinc");      
    }
  }
}
