using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Game;

namespace Character {
  public class Player : MovingObject {
    public Text timerText;
    public Text todoText;
    private Animator animator;
    private float time;
    private float maxSpeed = 0.25f;
    public GameObject moveCup;
    public AudioClip gather;
    private AudioSource gatherSound;
    private Transform lastPickup;
    private float idletime = 0f;

    private bool noMove = false;
    private bool noPickup = false;

    private int items = 0;
    private bool hamster = false;
    
    // Use this for initialization
    protected override void Start() {
      animator = GetComponent<Animator>();
      //timer = GameManager.instance.timer;
      //timerText.text = timer;
      setTodoText();
      base.Start();
      gatherSound = gameObject.AddComponent<AudioSource>();
      gatherSound.clip = gather;
      GameManager.instance.hamster = false;
    }


    private void setTodoText() {
      string x = (hamster) ? "X" : " ";
      todoText.text = items + "/5 Items Collected\n["+x+"]   Hamster";

    }
    
    // Update is called once per frame
    private void FixedUpdate() {

      //Handle timer
      time += Time.deltaTime;
      float minutes = Mathf.Floor(time/60);
      int seconds = Mathf.RoundToInt(time%60);
      string second = (seconds < 10) ? "0" + seconds.ToString() : seconds.ToString();
      timerText.text = minutes + ":" + second;

      //Handle movement
      float hor = 0;
      float vert = 0;
#if UNITY_STANDALONE || UNITY_STANDALONE_WIN
      hor = (Input.GetAxisRaw("Horizontal"));
      vert = (Input.GetAxisRaw("Vertical"));

      if (Input.GetButton("Fire1")) {
        getAngle(Input.mousePosition, out hor, out vert);
      } else {
        noMove = false;
      }

#else
      if (Input.touchCount > 0) {
        getAngle(Input.GetTouch(0).position, out hor, out vert);
      } else {
        noMove = false;
      }
#endif
      if (hor != 0 || vert != 0) {
        if (!noMove) {
          idletime = 0f;
          AttemptMove(hor*maxSpeed, vert*maxSpeed);
        } else {
          moveCup.transform.position = new Vector3(0f,0f,-9999f);
        }
        noPickup = true;
      } else {
        idletime += Time.deltaTime;
        if (idletime > 10f) {
          animator.SetTrigger("scratch");
          idletime = 0f;
        }
        animator.SetInteger("hor", 0);
        animator.SetInteger("vert", 0);        
        moveCup.transform.position = new Vector3(0f,0f,-9999f);
        noPickup = false;
      }
    }

    protected IEnumerator TouchMove() {
      yield return null;
    }

    protected void getAngle(Vector3 touchPosition, out float hor, out float vert) {
      double halfScreen = Screen.width * 0.5;
      double halfScreenHeight = Screen.height * 0.5;

      int layerMask = 1 << 8;
      RaycastHit2D hit = Physics2D.Raycast 
        (Camera.main.ScreenToWorldPoint(touchPosition), Vector2.zero, Mathf.Infinity, layerMask);
      if (hit.collider != null && !noPickup && canPickup(hit.collider)) {
        hor = 0;
        vert = 0;
        return;
      }
      Vector3 tp = Camera.main.ScreenToWorldPoint(touchPosition);
      moveCup.transform.position = new Vector3(tp.x, tp.y, -2f);

      float hMag = (float)(touchPosition.x - halfScreen);
      float vMag = (float)(touchPosition.y - halfScreenHeight);

      float mag = 1 / Mathf.Sqrt(Mathf.Pow(hMag,2f) + Mathf.Pow(vMag,2f));

      hor = hMag * mag;
      vert = vMag * mag;

      if (hMag > -20f && hMag < 20f) hor = 0;
      else if (hMag > -100f && hMag < 100f) hor = hor * Mathf.Pow(hMag/100f, 2f);

      if (vMag > -20f && vMag < 20f) vert = 0;
      if (vMag > -100f && vMag < 100f) vert = vert * Mathf.Pow(vMag/100f, 2f);

    }

    protected bool canPickup(Collider2D collider) {
      if (Mathf.Abs(character.gameObject.transform.position.x - collider.gameObject.transform.position.x) < 5f &&
          Mathf.Abs(character.gameObject.transform.position.y - collider.gameObject.transform.position.y) < 5f) {
        if (collider.gameObject.tag == "hamster") {
          hamster = true;
          GameManager.instance.hamster = true;
          collider.gameObject.GetComponent<Animator>().SetTrigger("noham");
          collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
          animator.SetTrigger("hamster");
        } else {

          gatherSound.pitch = Mathf.Lerp(1f, 1.4f, (float)items / 4f);
          items++;

          if (items != 5) {
            gatherSound.Play();
            gatherSound.Play(44100);
          }
          if (items == 5) {            
            Application.LoadLevel("points");
          }
          //Destroy(collider.gameObject);
          if (!lastPickup) lastPickup = transform;
          collider.gameObject.GetComponent<Animator>().enabled = false;
          collider.gameObject.GetComponent<Follower>().target = lastPickup;
          lastPickup = collider.gameObject.transform;
          collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
          Vector3 pos = collider.gameObject.transform.position;
          collider.gameObject.transform.position = new Vector3(pos.x, pos.y,0.2f);
          Vector3 scale = collider.gameObject.transform.localScale;
          collider.gameObject.transform.localScale = new Vector3(scale.x * 0.8f, scale.y * 0.8f);
        }
        setTodoText();

        noMove = true;
        return true;
      }
      return false;
    }

    protected override void AttemptMove(float xDir, float yDir) {
      base.AttemptMove(xDir, yDir);
      RaycastHit2D hit;

      if (Move(xDir, yDir, out hit)){
        int hor = 0;
        int vert = 0;
        if (xDir > 0) hor = 1;
        if (xDir < 0) hor = -1;
        if (yDir > 0) vert = 1;
        if (yDir < 0) vert = -1;
        animator.SetInteger("hor", hor);
        animator.SetInteger("vert", vert);
      } else {
        animator.SetInteger("hor", 0);
        animator.SetInteger("vert", 0);        
        moveCup.transform.position = new Vector3(0f,0f,-9999f);
      }
    }
  }
}
