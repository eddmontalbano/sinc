﻿using UnityEngine;
using System.Collections;

namespace Game {
  using System.Collections.Generic;
  using UnityEngine.UI;

  public class GameManager : MonoBehaviour {
    public class Task {
      public bool complete;
      public string text;
      public int priority;
      public bool hidden;
      public Task(string txt, int prior, bool hide) {
        text = txt;
        priority = prior;
        hidden = hide;
      }
    }

    public float refreshRate = 0.05f;
    public int gameTime = 180;
    public static GameManager instance = null;
    public bool hamster = false;

    private Text timer;
    private List<Task> tasks;
    private GameObject levelImage;
    private LevelManager levelScript;

    private bool doingSetup = true;


    void Awake() {
      if (instance == null)
        instance = this;
      else if (instance != this)
        Destroy(gameObject);
      DontDestroyOnLoad(gameObject);
      levelScript = GetComponent<LevelManager>();
      InitGame();
    }

    void OnLevelLoad(int index) {
      InitGame();
    }

    void InitGame() {
      doingSetup = true;
      //levelImage = GameObject.Find("LevelImage");
      //levelText = GameObject.Find("LevelText").GetComponent<Text>();
      //levelImage.SetActive(true);

      //levelScript.SetupScene();
    }

    void Update() {
      if (doingSetup)
        return;

      //StartCoroutine
    }
  }
}