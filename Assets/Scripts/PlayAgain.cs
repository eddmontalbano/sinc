﻿using UnityEngine;
using System.Collections;

public class PlayAgain : MonoBehaviour {
  public bool touched = false;
  private Vector3 touchPosition;

  
  private void FixedUpdate() {
    #if UNITY_STANDALONE || UNITY_STANDALONE_WIN
    
    if (Input.GetButton("Fire1")) {
      touched = true;
      touchPosition = Input.mousePosition;
      print("here");
    }
    #else
    if (Input.touchCount > 0) {
      touched = true;
      touchPosition = Input.GetTouch(0).position;    
    }
    #endif
    
    if (touched) {
      RaycastHit2D hit = Physics2D.Raycast(touchPosition, Vector2.zero, Mathf.Infinity);
      if (hit.collider != null) {
        Application.LoadLevel("sinc");
      }
    }
  }
}
